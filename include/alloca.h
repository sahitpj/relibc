#ifndef _ALLOCA_H
#define _ALLOCA_H

#include <alloca.h>

void *alloca(size_t size);

#endif